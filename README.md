# Sample Project: Wiki Navigation Issue

The Wiki component currently generates extra artifacts in the navigation hierarchy when using nested references and when pages of the same name are created.

View the [Wiki](https://gitlab.com/ugen-james/wiki-issue-demo/-/wikis/home) for the example.